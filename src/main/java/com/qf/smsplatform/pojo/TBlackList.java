package com.qf.smsplatform.pojo;


public class TBlackList {

  private long id;
  private String mobile;
  private long owntype;
  private long creater;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public String getMobile() {
    return mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }


  public long getOwntype() {
    return owntype;
  }

  public void setOwntype(long owntype) {
    this.owntype = owntype;
  }


  public long getCreater() {
    return creater;
  }

  public void setCreater(long creater) {
    this.creater = creater;
  }

}
