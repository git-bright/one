package com.qf.smsplatform.pojo;


import org.springframework.util.ObjectUtils;

public class TSmsTemplate implements CheckNull {

  private Long id;
  private String template;
  private String paramter;
  private Long owntype;
  private Long creater;
  private Long status;

  @Override
  public boolean isNull(CheckType type) {
    switch (type){
      case ADD:
        return ObjectUtils.isEmpty(template) || ObjectUtils.isEmpty(paramter) || ObjectUtils.isEmpty(owntype) || ObjectUtils.isEmpty(creater) || ObjectUtils.isEmpty(status);
    }
    return CheckNull.super.isNull(type);
  }

  @Override
  public String toString() {
    return "TSmsTemplate{" +
            "id=" + id +
            ", template='" + template + '\'' +
            ", paramter='" + paramter + '\'' +
            ", owntype=" + owntype +
            ", creater=" + creater +
            ", status=" + status +
            '}';
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTemplate() {
    return template;
  }

  public void setTemplate(String template) {
    this.template = template;
  }

  public String getParamter() {
    return paramter;
  }

  public void setParamter(String paramter) {
    this.paramter = paramter;
  }

  public Long getOwntype() {
    return owntype;
  }

  public void setOwntype(Long owntype) {
    this.owntype = owntype;
  }

  public Long getCreater() {
    return creater;
  }

  public void setCreater(Long creater) {
    this.creater = creater;
  }

  public Long getStatus() {
    return status;
  }

  public void setStatus(Long status) {
    this.status = status;
  }
}
