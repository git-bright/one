package com.qf.smsplatform.pojo;


public class TNotify {

  private Long id;
  private String tag;
  private String desp;
  private Long notifyState;
  private Long cacheState;


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTag() {
    return tag;
  }

  public void setTag(String tag) {
    this.tag = tag;
  }

  public String getDesp() {
    return desp;
  }

  public void setDesp(String desp) {
    this.desp = desp;
  }

  public Long getNotifyState() {
    return notifyState;
  }

  public void setNotifyState(Long notifyState) {
    this.notifyState = notifyState;
  }

  public Long getCacheState() {
    return cacheState;
  }

  public void setCacheState(Long cacheState) {
    this.cacheState = cacheState;
  }
}
