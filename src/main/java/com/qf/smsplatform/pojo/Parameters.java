package com.qf.smsplatform.pojo;


public class Parameters {

  private long id;
  private String name;
  private String params;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  public String getParams() {
    return params;
  }

  public void setParams(String params) {
    this.params = params;
  }

  @Override
  public String toString() {
    return "Parameters{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", params='" + params + '\'' +
            '}';
  }
}
